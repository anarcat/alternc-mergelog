# Merge apache logs from differents servers
# Must be launched AFTER logrotate so it's here instead of cron.daily

0 7 * * * www-data /usr/sbin/alternc-mergelog
